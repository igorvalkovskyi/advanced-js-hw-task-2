const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];
function isValidBook(book) {
    return book.hasOwnProperty("author") && book.hasOwnProperty("name") && book.hasOwnProperty("price");
  }
  
  function renderBooks() {
    const rootDiv = document.getElementById("root");
    const ul = document.createElement("ul");
  
    books.forEach((book) => {
      if (isValidBook(book)) {
        const li = document.createElement("li");
        li.textContent = `Author: ${book.author || "Unknown"}, Name: ${book.name || "Unknown"}, Price: ${book.price || "Unknown"}`;
        ul.appendChild(li);
      } else {
        console.error("Invalid book object:", book);
      }
    });
  
    rootDiv.appendChild(ul);
  }
  
  renderBooks();